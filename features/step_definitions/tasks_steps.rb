# coding: utf-8

Given("I'm in the task registry screen") do
     #@task = TaskScreen.new
     #@task.acess_task_screen
     @browser.goto "https://qa-test.avenuecode.com/tasks"
end

When("add a task") do
    #@task.add_task
    @browser.text_field(id:"new_task").set "Task test"
    @browser.input(id:"new_task").send_keys :enter
end

When("remove a task") do
    #@task.remove_task
    @browser.button(class: 'btn btn-xs btn-danger').click                
end

Then("I should see the task added successfully") do
    #fail "Error, unable to add task" unless @task.validate_task_add                    
    fail 'Error, Unable add task' unless @browser.text.include?'Manage Subtasks'                    
end

Then("I should see the task removed successfully") do
    #fail "Error, unable remove task" unless @task.validate_task_remove                        
    puts 'pending'
end