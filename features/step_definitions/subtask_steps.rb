# coding: utf-8

When("access a subtask") do
    #@subtask = SubTaskScreen.new
    #@substak.acess_subtask
    @browser.button(class: 'btn btn-xs btn-primary ng-binding').click
end

When("add a subtask") do
    #@subtask.add_subtask
    #@subtask.add_subtask
    @browser.text_field(id: 'new_sub_task').set "Subtarefa"
    @browser.button(id: 'add-subtask').click                    
end

When("remove a subtask") do
    #@subtask.remove_subtask
    puts 'pending'
end

Then("I should see the subtask added successfully") do
    #fail "Error, unable add a subtask" unless @subtask.validate_subtask_add                        
     puts 'pending'
end

Then("I should see the subtask removed successfully") do
    #fail "Error, unable remove a subtask" unless @subtask.validate_subtask_add                            
    fail "Error, unable remove a subtask" unless @browser.text.include?"Remove SubTask"                    
end
