# coding: utf-8

Feature: Tasks

    As a user,
    I should be able to add and remove a task 
    So I can control my tasks

    @build
    Scenario: Add Task successful
        Given I'm in the task registry screen 
        When add a task
        Then I should see the task added successfully

    @build
    Scenario: Remove Task successful
        Given I'm in the task registry screen  
        When remove a task
        Then I should see the task removed successfully