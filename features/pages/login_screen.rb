#coding: utf-8

require 'watir'
require 'rspec'

class LoginScreen
     def initialize
         @user_email = 'user_email'
         @user_password = 'user_password'
         @btn_return = 'btn btn-primary'
     end

    def login_url
         $browser.goto "https://qa-test.avenuecode.com/users/sign_in"       
    end
   
    def  enter_user
         @browser.text_field(id: @user_email).set LOGIN[:email]       
    end

    def enter_password
         @browser.text_field(id: @user_password).set LOGIN[:password]        
    end

    def touch_return
         @browser.button(id: @btn_return).click       
    end

    def validate_login
         @browser.text.include?"Signed in successfully."    
    end
end