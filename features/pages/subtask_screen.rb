#coding: utf-8

require 'watir'
require 'rspec'

class SubTaskScreen
    def initialize
        @acess_subtask = 'btn btn-xs btn-primary ng-binding'
        @new_sub_task = 'new_subtask'
        @btn_add_subtask = 'add-subtask'
    end

    def acess_subtask
        @browser.button(class: @acess_subtask).click
    end
   
    def  add_subtask
        @browser.text_field(id: @new_subtask).set SUBTASK[:subtask]
        @browser.button(id: @btn_add_subtask).click      
    end

    def remove_subtask
        @browser.button(class: @btn_remove_task).click                
    end

    def validate_subtask_add
        #@browser.text.include?"Manage Subtasks"
    end

    def validate_subtask_remove
        #pending
    end
end

