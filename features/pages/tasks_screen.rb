#coding: utf-8

require 'watir'
require 'rspec'

class TaskScreen
    def initialize
        @new_task = 'new_task'
        @btn_remove_task = 'btn btn-xs btn-danger'
        @remove_task = ''
    end

    def acess_task_screen
        @browser.goto "https://qa-test.avenuecode.com/tasks"
    end
   
    def  add_task
        @browser.text_field(id: @new_task).set TASK[:task]
        @browser.input(id: @new_task).send_keys :enter      
    end

    def remove_task
        @browser.button(id: @remove_task).click
        #pending                
    end

    def validate_task_add
        #pending
    end

    def validate_subtask_remove
        #pending
    end
end