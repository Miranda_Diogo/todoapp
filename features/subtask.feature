# coding: utf-8

Feature: Subtasks

As a user
    I should be able add, and remove a subtask
    So I can manager my subtasks

    @build
    Scenario: Add Subtask successful
        Given I'm in the task registry screen
        When access a subtask 
        And add a subtask
        Then I should see the subtask added successfully

    @pending
    Scenario: Remove Subtask successful
        Given I'm in the task registry screen
        When access a subtask
        And remove a subtask
        Then I should see the subtask removed successfully

