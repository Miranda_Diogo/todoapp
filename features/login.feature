# coding: utf-8

Feature: Login

    As a User,
    I should be able to provide email and password
    So I can acess ToDo App

    @build
    Scenario: Login successful
        Given I'm in login screen
        When I login with a valid user
        Then I successfully login